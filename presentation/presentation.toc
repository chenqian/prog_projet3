\beamer@endinputifotherversion {3.10pt}
\select@language {french}
\beamer@sectionintoc {1}{Les impl\'ementations de base}{3}{0}{1}
\beamer@subsectionintoc {1}{1}{Classes utilis\IeC {\'e}es}{4}{0}{1}
\beamer@sectionintoc {2}{Coloriage et Lambert}{5}{0}{2}
\beamer@subsectionintoc {2}{1}{Le premier coloriage: Mod\`ele de Lambert}{6}{0}{2}
\beamer@sectionintoc {3}{Anti-aliasing et SSAA}{8}{0}{3}
\beamer@subsectionintoc {3}{1}{Int\'er\^et de l'anti-aliasing}{9}{0}{3}
\beamer@subsectionintoc {3}{2}{R\'ealisation d'anti-aliasing avec SSAA:SuperSample AntiAliasing}{10}{0}{3}
\beamer@sectionintoc {4}{Ombrage}{11}{0}{4}
\beamer@subsectionintoc {4}{1}{D\'etection de l'ombrage}{12}{0}{4}
\beamer@sectionintoc {5}{Influence de la couleur de la source}{13}{0}{5}
