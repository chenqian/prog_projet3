\subsection{Loi de Lambert}

\subsubsection{Modèle correspondant au modèle de Lambert}
	Modèle de Lambert correspond à la surface mate. Mais le défaut de ce modèle est que nous ne pouvons pas 
	représenter l'influence de la couleur de la source.
	
\subsubsection{Calcul de la luminosité pour le modèle de Lambert}
	\begin{myTh} {\emph{Modèle de Lambert :}}
		La luminosité est proportionnelle au cosinus de l'angle entre le rayon incident et le vecteur normal à la 
		surface.
	\end{myTh}
	\begin{figure}[H]
		\centering
		\includegraphics[width= 0.5\textwidth,natwidth=10,natheight=60]{images/lambert.png}
		\caption{Schéma pour le modèle de Lambert}
	\end{figure}
	


\subsection{Coloration par la source lumineuse}

Comme dit précédemment, la loi de Lambert n'est pas suffisante pour le coloriage des rayons. 
Celle-ci ne prend pas du tout en compte l'influence de la source lumineuse, qui est colorée.

C'est pourquoi il a fallu ajouter une nouvelles fonction dans \emph{change\_color} pour prendre en compte cet effet.7

Cette la fonction \emph{change\_color\_source}.

Voici donc le problème au point où on en est :

Le rayon a intercepté un objet de couleur $A$. Avant d'appliquer à cette couleur le coefficient de Lambert, il serait préférable
de trouver un moyen de prendre la couleur $B$ de la source lumineuse !

Pour cela, quelles sont les informations à prendre en compte ? Bien évidemment, la couleur de l'objet et celle de la source ..
Mais ce n'est pas suffisant, si l'on essaye d'éclairer une sphère blanche par une lumière rouge, la sphère deviendra rose.
Et plus la lumière sera loin de l'objet, plus le rose sera clair. C'est pour cela que l'on a décidé de prendre en compte la
valeur $\frac{1}{d}$, où $d$ est la distance entre l'objet et la source de lumière (on prend l'inverse afin de rendre compte
que l'influence de la lumière sera moins grande quand elle sera loin).

Au départ nous avons donc calculé le barycentre $tA+(1-t)B$, où $t=\frac{1}{d^2}$ (les lois physiques étant quadratiques, cela
semblait raisonnable). Cela peut sembler étrange que $A$ n'ait plus aucune influence si on colle la source lumineuse à l'objet,
mais c'est bien l'impression que donne la réalité (si on colle un laser rouge sur un objet blanc on a bien l'impression
que celui ci devient rouge).

Mais alors la lumière n'avait pas suffisamment de poids, il fallait qu'elle soit quasiment collée à la sphère pour que celle-ci
prenne vraiment une teinte rose.

Il nous a été proposé d'affecter à $t$ un grand coefficient $k$, pour augmenter sa valeur. Mais alors les changements que l'on 
doit appliquer à notre formule pour qu'elle continue à calculer un barycentre (par exemple, remplacer $1-kt$ par $k-kt$,
pour éviter que les couleurs deviennent négatives, et diviser le tout par $(k-kt)+kt=k$, la somme des poids, sinon les couleurs
deviennent trop grandes à partir d'une certaine distance et repartirait de $0$ ..) rendent ce coefficient obsolète.

Il aurait également été possible d'affecter le rayon d'une nouvelle composante : son énergie, qui diminuerait avec la distance,
pour rester dans le côté ``physique'' du programme.

Mais nous avons choisi plus simplement de faire décroître $t$ moins vite en fonction de $d$. Par exemple en lui appliquant
la racine carrée. Ce n'était pas suffisant, et après divers essais nous avons choisi de manière complètement inductive
la valeur $t=\frac{1}{\sqrt[6]{d}}$.