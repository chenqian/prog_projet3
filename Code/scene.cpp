#include "scene.hpp"
#include <vector>
#include <iostream>
#include <cmath>
using namespace rt;

Scene::Scene ()
{
	std::list<Obj*> aux;
	objets = aux;
}

Scene::Scene (const Scene& other) :objets (other.objets)
{
}

void Scene::add_obj (Obj* obj_ref)
{
	objets.list::push_front(obj_ref);
}

Obj* Scene::get_obj ()
{
	Obj* obj_front (objets.front());
	objets.list::pop_front();
	return (obj_front);
}

Obj* Scene::fst_obj (Rayon ray)
{
	double lambda_min =  0;
	double aux = 0;
	Obj *obj_min;
	for (std::list<Obj*>::iterator it= objets.begin(); it!=objets.end(); ++it)
	{
		aux = (*it) -> lambda(ray);

		if ((aux > 0) && ((aux < lambda_min) || (lambda_min <= 0)))
		{
			lambda_min = aux;
			obj_min = *it;
		}
		
	}
	if (lambda_min <= 0.001)
	return *objets.begin();
	else
	return obj_min;
}




void Scene::change_color_primary (Rayon& ray,Light source,Obj* obj)
{
	double ReflexCoeff = (obj->CoeffReflex(ray,source));
	color newcolor (ray.get_color().get_red()*ReflexCoeff,ray.get_color().get_green()*ReflexCoeff,ray.get_color().get_blue()*ReflexCoeff,ray.get_color().get_alpha());
	if (obj->lambda(ray) >= 0.001) 
	{
		ray.set_color(newcolor);
	}
}



void Scene::change_color_source (Rayon& ray, Light source,Obj* obj)
{
	vector inter = obj->IntersectRay(ray);
	Rayon newray = Rayon(source.get_origin(), (inter-source.get_origin()).unit(), ray.get_color() );
	Obj *sec (this->fst_obj(newray));
	double d( pow( ( (source.get_origin()-inter).norm() ), (-1./6.) ) );
	unsigned char r(((1-d)*obj->get_color().get_red()+d*source.get_color().get_red()) );
	unsigned char g(((1-d)*obj->get_color().get_green()+d*source.get_color().get_green()) );
	unsigned char b(((1-d)*obj->get_color().get_blue()+d*source.get_color().get_blue()) );
	color newcolor (r,g,b,obj->get_color().get_alpha());
	//std::cout<< sec->lambda(newray)<<" "<<obj->lambda(ray)<<"        ";
	
	if ((obj->lambda(ray) >= 0.001) && (pow((source.get_origin()-inter).norm()- sec->lambda(newray),2) < 0.001))
	{
		ray.set_color(newcolor);
	}
}

void Scene::change_color_ray (Rayon& ray,Light source)
{
	Obj *obj (this->fst_obj(ray));
	change_color_source (ray,source,obj);
	change_color_primary (ray,source,obj);
	
	
	
}
void Scene::antialiasing4_ray(image& im)
{
	int h = im.height();
	int w = im.width();
	for (int j = 0; j <= (h); j++)
	{
		for (int i=0; i<=(w);i++)
		{
			double r = (im.get_pixel(i,j).get_red() + im.get_pixel(i-1,j).get_red() + im.get_pixel(i+1,j).get_red() + im.get_pixel(i,j-1).get_red() + im.get_pixel(i,j+1).get_red())/5;
			//std::cout<<im.get_pixel(i,j).get_red() + im.get_pixel(i-1,j).get_red()<<" ";
			//std::cout<<im.get_pixel(i,j).get_green() + im.get_pixel(i-1,j).get_green()<<" ";
			double g = (im.get_pixel(i,j).get_green() + im.get_pixel(i-1,j).get_green() + im.get_pixel(i+1,j).get_green() + im.get_pixel(i,j-1).get_green() + im.get_pixel(i,j+1).get_green())/5;
			double b = (im.get_pixel(i,j).get_blue() + im.get_pixel(i-1,j).get_blue() + im.get_pixel(i+1,j).get_blue() + im.get_pixel(i,j-1).get_blue() + im.get_pixel(i,j+1).get_blue())/5;
			double a = (im.get_pixel(i,j).get_alpha() + im.get_pixel(i-1,j).get_alpha() + im.get_pixel(i+1,j).get_alpha() + im.get_pixel(i,j-1).get_alpha() + im.get_pixel(i,j+1).get_alpha())/5;
			im.set_pixel(i,j,color (r,g,b,a));
		}
	}
}

void Scene::antialiasing16_ray(image& im)
{
	int h = im.height();
	int w = im.width();
	for (int i = 0; i <= (w); i++)
	{
		for (int j=0; j<=(h);j++)
		{
			unsigned char r = (im.get_pixel(i,j).get_red() + im.get_pixel(i-1,j).get_red() + im.get_pixel(i+1,j).get_red() + im.get_pixel(i,j-1).get_red() + im.get_pixel(i,j+1).get_red()+ im.get_pixel(i,j-2).get_red()+ im.get_pixel(i,j+2).get_red()+ im.get_pixel(i+2,j).get_red()+ im.get_pixel(i-2,j).get_red()+im.get_pixel(i-1,j-1).get_red() + im.get_pixel(i-1,j+1).get_red() + im.get_pixel(i+1,j-1).get_red() + im.get_pixel(i+1,j+1).get_red()+im.get_pixel(i-2,j-2).get_red() + im.get_pixel(i-2,j+2).get_red() + im.get_pixel(i+2,j+2).get_red() + im.get_pixel(i+2,j-2).get_red())/17;
			unsigned char g = (im.get_pixel(i,j).get_green() + im.get_pixel(i-1,j).get_green() + im.get_pixel(i+1,j).get_green() + im.get_pixel(i,j-1).get_green() + im.get_pixel(i,j+1).get_green()+ im.get_pixel(i,j-2).get_green()+ im.get_pixel(i,j+2).get_green()+ im.get_pixel(i+2,j).get_green()+ im.get_pixel(i-2,j).get_green()+im.get_pixel(i-1,j-1).get_green() + im.get_pixel(i-1,j+1).get_green() + im.get_pixel(i+1,j-1).get_green() + im.get_pixel(i+1,j+1).get_green()+im.get_pixel(i-2,j-2).get_green() + im.get_pixel(i-2,j+2).get_green() + im.get_pixel(i+2,j+2).get_green() + im.get_pixel(i+2,j-2).get_green())/17;
			unsigned char b = (im.get_pixel(i,j).get_blue() + im.get_pixel(i-1,j).get_blue() + im.get_pixel(i+1,j).get_blue() + im.get_pixel(i,j-1).get_blue() + im.get_pixel(i,j+1).get_blue()+ im.get_pixel(i,j-2).get_blue()+ im.get_pixel(i,j+2).get_blue()+ im.get_pixel(i+2,j).get_blue()+ im.get_pixel(i-2,j).get_blue()+im.get_pixel(i-1,j-1).get_blue() + im.get_pixel(i-1,j+1).get_blue() + im.get_pixel(i+1,j-1).get_blue() + im.get_pixel(i+1,j+1).get_blue()+im.get_pixel(i-2,j-2).get_blue() + im.get_pixel(i-2,j+2).get_blue() + im.get_pixel(i+2,j+2).get_blue() + im.get_pixel(i+2,j-2).get_blue())/17;
			unsigned char a = (im.get_pixel(i,j).get_alpha() + im.get_pixel(i-1,j).get_alpha() + im.get_pixel(i+1,j).get_alpha() + im.get_pixel(i,j-1).get_alpha() + im.get_pixel(i,j+1).get_alpha()+ im.get_pixel(i,j-2).get_alpha()+ im.get_pixel(i,j+2).get_alpha()+ im.get_pixel(i+2,j).get_alpha()+ im.get_pixel(i-2,j).get_alpha()+im.get_pixel(i-1,j-1).get_alpha() + im.get_pixel(i-1,j+1).get_alpha() + im.get_pixel(i+1,j-1).get_alpha() + im.get_pixel(i+1,j+1).get_alpha()+im.get_pixel(i-2,j-2).get_alpha() + im.get_pixel(i-2,j+2).get_alpha() + im.get_pixel(i+2,j+2).get_alpha() + im.get_pixel(i+2,j-2).get_alpha())/17;
			im.set_pixel(i,j,color (r,g,b,a));

		}
	}
}

void Scene::ssaa4x (image& im, Camera ca,Light source)
{
	int h = im.height();
	int w = im.width();
	image im4x(w*2,h*2); 

	for (double i = 0; i<= w; i = i + 0.5)
	{
		for (double j=0;j<=h;j = j + 0.5)
		{
			Rayon ray (ca.ryPixel(i,j,color (25,25,25,255)));
			//std::cout<<ray.get_color().get_blue();
			this->change_color_ray(ray,source);
			unsigned char r = ray.get_color().get_red();
			unsigned char g = ray.get_color().get_green();
			unsigned char b = ray.get_color().get_blue();
			unsigned char a = ray.get_color().get_alpha();
			im4x.set_pixel(i*2,j*2,color(r,g,b,a));
			
		}
	}
	this->antialiasing4_ray(im4x);
	for (int i = 0; i<= w;i = i+1)
	{
		for (int j=0;j<=h;j = j+1)
		{
			im.set_pixel(i,j,im4x.get_pixel(i*2,j*2));
			//std::cout<<im.get_pixel(i,j);
		}
	}
}

void Scene::ssaa16x (image& im, Camera ca,Light source)
{
	int h = im.height();
	int w = im.width();
	image im16x(w*4,h*4); 
	std::cout<<source.get_color().get_blue()-1;
	std::cout<<source.get_color().get_red()-1;
	std::cout<<source.get_color().get_green()-1;
	for (double i = 0; i<= w*4; i = i + 1)
	{
		for (double j=0;j<=h*4;j = j + 1)
		{
			Rayon ray (ca.ryPixel(i/4,j/4,color (25,25,25,255)));
			

			this->change_color_ray(ray,source);
			unsigned char r = ray.get_color().get_red();
			unsigned char g = ray.get_color().get_green();
			unsigned char b = ray.get_color().get_blue();
			unsigned char a = ray.get_color().get_alpha();
			//std::cout<<ray.get_color().get_red()-1<<",";
			//std::cout<<ray.get_color().get_green()-1<<",";
			//std::cout<<ray.get_color().get_blue()-1<<"  ";
			//std::cout<<im16x.get_pixel(i,j).get_red()-1<<",";
			//std::cout<<im16x.get_pixel(i,j).get_green()-1<<",";
			//std::cout<<im16x.get_pixel(i,j).get_blue()-1<<"  ";
			im16x.set_pixel(i,j, color (r,g,b,a));
			//std::cout<<r-1<<" ";
			//std::cout<<im16x.get_pixel(i,j).get_red()-1<<",";
			//std::cout<<im16x.get_pixel(i,j).get_green()-1<<",";
			//std::cout<<im16x.get_pixel(i,j).get_blue()-1<<"  "<<std::endl;
		}
	}
	this->antialiasing16_ray(im16x);
	
	for (int i = 0; i<= w;i = i+1)
	{
		for (int j=0;j<=h;j = j+1)
		{
			
			
			im.set_pixel(i,j,im16x.get_pixel(i*4,j*4));
			//std::cout<<im.get_pixel(i,j);
		}
	}
}

void Scene::draw_primary (image& im, Camera ca,Light source)
{
	int h = im.height();
	int w = im.width();
	for (int i = 0; i<= w;i = i+1)
	{
		for (int j=0;j<=h;j = j+1)
		{
			Rayon ray (ca.ryPixel(i,j,color (25,25,25,255)));
			this->change_color_ray(ray,source);
			
			
			im.set_pixel(i,j,ray.get_color());
			
		}
	};
}

void Scene::draw_reflex (image& im,Camera ca,Light source)
{
	int h = im.height();
	int w = im.width();
	for (int i = 0; i<= w;i = i+1)
	{
		for (int j=0;j<=h;j = j+1)
		{
			Rayon ray (ca.ryPixel(i,j,color (25,25,25,255)));
			this->change_color_ray(ray,source);
			Obj* obj (this->fst_obj(ray));
			Rayon ray_reflex (obj->reflex_rayon(ray));
			Light new_source (source.get_origin(), ray_reflex.get_color());
			this ->change_color_ray(ray_reflex,new_source);
			color new_color (ray.get_color().get_red() *2/5 + ray_reflex.get_color().get_red() *3/5,
									ray.get_color().get_green() *2/5 + ray_reflex.get_color().get_green() *3/5,
									ray.get_color().get_blue() *2/5 + ray_reflex.get_color().get_blue() *3/5,
									ray.get_color().get_alpha() *2/5 + ray_reflex.get_color().get_alpha() *3/5);
			im.set_pixel(i,j,new_color);
			
		}
	};
}
