#pragma once
#include "vector.hpp"
#include "color.hpp"

using namespace rt;
class Light

/* La classe Lumiere sera simplement representée par l'origin de la source lumineuse et sa couleur */
{
	private: 
		rt::vector origin;
		rt::color LightColor;
	public:
		Light (rt::vector centre);
		Light (rt::vector centre, rt::color LightColor);
		rt::vector get_origin (void);
		rt::color get_color (void);
};
