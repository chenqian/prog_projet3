#include "rayon.hpp"
using namespace rt;
Rayon::Rayon()
{
	origin = vector (0,0,0);
	direction = vector (0,0,0);
	RayColor = color::WHITE; 
}

Rayon::Rayon(rt::vector orig, rt::vector dir,rt::color coulr) : origin(orig), direction(dir), RayColor(coulr)
{}

vector Rayon::get_origin() const {return origin;} 

vector Rayon::get_direction() const {return direction;}

color Rayon::get_color() const
{
	return (RayColor);
}

void Rayon::set_color(color RayColor)
{
	this->RayColor = RayColor;
}

