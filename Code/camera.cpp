#include "camera.hpp"
#include "color.hpp"
#include "image.hpp"
using namespace rt;

Camera::Camera(vector pos, double dist)
{
	position = pos;
	direction = vector(0,0,-1);
	distance = dist;
}

Camera::Camera(vector pos, vector dir, double dist)
{
	position = pos;
	direction = dir;
	distance = dist;
}

vector Camera::get_position() const {return position;}

vector Camera::get_direction() const {return direction;}

double Camera::get_distance() const {return distance;}

Rayon Camera::ryPixel(double posx, double posy,color RayColor) const
{
	double posz (position.z + distance);
	return Rayon(position, vector(posx,posy,posz)-position,RayColor);
}
