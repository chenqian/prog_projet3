#pragma once
#include "vector.hpp"
#include "color.hpp"
#include "obj.hpp"
#include "camera.hpp"
#include "light.hpp"
using namespace rt;
// la class surface represente les objets du type "plan"
namespace rt{
class Surface :public Obj
{
	private:
		vector origin;
		vector direction;
	public:
		Surface ();
		Surface (vector origin, vector direction, color couleur);
		vector IntersectRay (const Rayon& ray);
		double lambda (const Rayon& ray);
		double CoeffReflex(const Rayon& ray, Light source);
		Rayon reflex_rayon (const Rayon& ray);
		double CoeffPhong(const Rayon& ray, Light source,double coeff,Camera ca);
};
}
