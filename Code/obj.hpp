#pragma once
#include "color.hpp"
#include "rayon.hpp"
#include "camera.hpp"
#include "light.hpp"
#include "vector.hpp"

using namespace rt;
namespace rt{
class Obj
{
	protected:
		color obj_color;
	public:
		Obj ();
		Obj (const Obj& obj);
		Obj (color obj_color);
		virtual vector IntersectRay (const Rayon& ray) = 0;
		virtual double CoeffReflex(const Rayon& ray, Light source) = 0;
		//coeff is the reflexivity coefficient
		virtual double CoeffPhong(const Rayon& ray, Light source,double Coeff,Camera ca) = 0;
		virtual double lambda(const Rayon& ray) =0;
		virtual Rayon reflex_rayon (const Rayon& ray) =0;
		color get_color();
		
};
}
