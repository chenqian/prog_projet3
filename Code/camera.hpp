#pragma once
#include "rayon.hpp"
#include "vector.hpp"
#include "image.hpp"
namespace rt {
    
    /* On représente l'objet Camera par :
        - la position de la Camera dans l'espace, sous forme de vecteur
        - la direction vers laquelle on dirige la Camera, toujours avec un vecteur
        - la distance de la Camera à l'image, sous forme de double
        - ryPixel renvoie un rayon issue de camera pointe sur un pixel donne dans l'image
    */
    class Camera
    {
        private :
            rt::vector position;
            rt::vector direction;
            double distance;
        public :
			Camera(rt::vector position,double distance);
            Camera(rt::vector position, rt::vector direction, double distance);
            rt::vector get_position() const;
            rt::vector get_direction() const;
            double get_distance() const;
            rt::image get_picture() const;
            rt::Rayon ryPixel(double posx, double posy,color RayColor) const;
    };
}
