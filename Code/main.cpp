#include <iostream>
#include <unistd.h>
#include <list>
#include "vector.hpp"
#include "screen.hpp"
#include "rayon.hpp"
#include "color.hpp"
#include "sphere.hpp"
#include "light.hpp"
#include "surface.hpp"
#include "scene.hpp"
#include <list>
using namespace rt; 
using namespace std;




int main(void)
{
	rt::screen s(640,480);
	/*
	image im16x (640,480);
	for ( int i = 0; i< 640; i++)
		for (int j = 0; j< 480; j++)
		{
			im16x.set_pixel(i,j,color::BLUE);
			std::cout<<im16x.get_pixel(i,j).get_red()-1<<",";
			std::cout<<im16x.get_pixel(i,j).get_green()-1<<",";
			std::cout<<im16x.get_pixel(i,j).get_blue()-1<<"  "<<endl;
		}*/
	// here the code to draw the pixels...
	
	Sphere sp (vector(320,240,500),100,color::RED);
	Sphere sp2 (vector(380,140,300),100,color::WHITE);
	Sphere sp3 (vector(500,240,800),100,color::BLUE);
	Camera ca (vector(320,240,-500), 800);
	Light source (vector(300,-200,0),color::WHITE);
	Surface su (vector (320,340,-500),vector(0,-1,0),color::WHITE);
	Surface su2 (vector (320,340,1500),vector(0,0,-1),color::RED);
	Surface su3 (vector (800,340,1500),vector(-1,0,0),color::GREEN);
	Scene scene;
	scene.add_obj(&sp);
	scene.add_obj(&sp2);
	scene.add_obj(&sp3);
	scene.add_obj(&su);
	scene.add_obj(&su2);
	
	scene.add_obj(&su3);
	//scene.draw_primary(s,ca,source);
	//scene.draw_reflex(s,ca,source);
	//scene.draw_primary(s,ca,source);
	scene.ssaa16x(s,ca,source);
	
	cout<<"Printfinished!";
	while(s.update()) {
		// wait for an input from user
	}
	
	return 0;
}
