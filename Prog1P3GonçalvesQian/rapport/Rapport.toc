\select@language {french}
\contentsline {paragraph}{Mots-cl\IeC {\'e}s~:}{1}{section*.1}
\contentsline {section}{\numberline {1}Introduction}{1}{section.1}
\contentsline {section}{\numberline {2}Impl\'ementations de diff\'erentes classes}{1}{section.2}
\contentsline {section}{\numberline {3}Coloriage de Lambert}{2}{section.3}
\contentsline {subsection}{\numberline {3.1}Mod\IeC {\`e}le correspondant au mod\IeC {\`e}le de Lambert}{2}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Calcul de la luminosit\IeC {\'e} pour le mod\IeC {\`e}le de Lambert}{2}{subsection.3.2}
\contentsline {section}{\numberline {4}Anti-Aliasing et SSAA}{2}{section.4}
\contentsline {subsection}{\numberline {4.1}Int\IeC {\'e}r\IeC {\^e}t et utilit\IeC {\'e} de l'Anti-Aliasing}{2}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Premi\IeC {\`e}re tentation }{2}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}SSAA(SuperSample Anti-Aliasing)}{3}{subsection.4.3}
\contentsline {subsubsection}{\numberline {4.3.1}Repr\IeC {\'e}sentation de SSAA}{3}{subsubsection.4.3.1}
\contentsline {subsubsection}{\numberline {4.3.2}Impl\IeC {\'e}mentations de SSAA}{3}{subsubsection.4.3.2}
\contentsline {section}{\numberline {5}Ombrage}{3}{section.5}
\contentsline {subsection}{\numberline {5.1}Approche simple}{3}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Probl\IeC {\`e}me rencontr\IeC {\'e} \IeC {\`a} cause de la pr\IeC {\'e}cision de double}{4}{subsection.5.2}
\contentsline {subsection}{\numberline {5.3}Nouvelle approche}{4}{subsection.5.3}
\contentsline {section}{\numberline {6}Influence de la couleur de la source}{4}{section.6}
\contentsline {section}{\numberline {7}Conclusion}{4}{section.7}
