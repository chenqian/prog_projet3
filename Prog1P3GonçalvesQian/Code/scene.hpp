#pragma once
#include "obj.hpp"
#include <list>
#include "light.hpp"
#include "color.hpp"
#include "screen.hpp"

using namespace rt;

namespace rt{
class Scene
{
	private:
		std::list<Obj*> objets;
		Obj* fst_obj (Rayon ray);
	public:
		Scene ();
		Scene(const Scene& other);
		void add_obj (Obj* obj_ref);
		Obj* get_obj ();
		void change_color_primary(Rayon& ray,Light source,Obj* obj);
		void change_color_source (Rayon& ray,Light source,Obj* obj);
		void change_color_ray (Rayon& ray,Light source);
		void antialiasing4_ray (image& im);
		void antialiasing16_ray (image& im);
		void ssaa4x (image& im, Camera ca,Light source);
		void ssaa16x (image& im, Camera ca,Light source);
		void draw_reflex (image& im,Camera ca,Light source);
		void draw_primary (image& im, Camera ca,Light source);
};
}

