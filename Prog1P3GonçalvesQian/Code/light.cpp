#include "light.hpp"

Light :: Light (rt::vector vec)
{
	origin = vec;
        LightColor = rt::color::WHITE;
}

Light :: Light (rt::vector centre, rt::color col) 
{
	origin = centre;
	LightColor = col;
}

rt::vector Light::get_origin(void)
{
	return origin;
}

rt::color Light :: get_color(void)
{
        return LightColor;
}
