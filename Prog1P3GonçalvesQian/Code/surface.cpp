#include "surface.hpp"
#include "color.hpp"
#include <iostream>

using namespace rt;

Surface::Surface() :Obj(color::WHITE)
{
	origin = vector ();
	direction = vector ();
}

Surface::Surface(vector origin, vector direction,color couleur) :Obj(couleur)
{
	this->origin = origin;
	this->direction = direction;
}

vector Surface::IntersectRay (const Rayon& ray)
{
	vector RayOrigin = ray.get_origin();
	vector RayDirection = ray.get_direction();
	double coeff = Surface::lambda(ray);
	return (coeff * RayDirection + RayOrigin);
}

double Surface::lambda (const Rayon& ray)
{
	vector RayOrigin = ray.get_origin();
	vector RayDirection = ray.get_direction();
	double lambda = (origin.x*direction.x+origin.y*direction.y+origin.z*direction.z-RayOrigin.y*direction.y-RayOrigin.z*direction.z-RayOrigin.x*direction.x)/(direction.x*RayDirection.x+direction.y*RayDirection.y+direction.z*RayDirection.z);
	
	//std::cout << lambda <<" ";
	if ((RayDirection | direction) < 0) 
	{
		return (lambda);
	}
	else
	{
		return 0;
	}
}

double Surface::CoeffReflex(const Rayon& ray, Light source)
{
	vector RayOrigin = ray.get_origin();
	vector RayDirection = ray.get_direction();
	double coeff = Surface::lambda(ray);
	if (coeff >0)
	{vector PointIntersect = RayOrigin + coeff* RayDirection;
	//std::cout << (PointIntersect.x) <<" "<<(PointIntersect.y) <<" "<< (PointIntersect.z)<<std::endl;
	vector LumIncident = (source.get_origin() - PointIntersect).unit();
	//std::cout<<(LumIncident|direction.unit());
	return (LumIncident|direction.unit());}
	else
	{
		return 0;
	}
}

Rayon Surface::reflex_rayon (const Rayon& ray)
{
	vector intersect (this->Surface::IntersectRay(ray));
	vector normal (direction);
	Rayon rayon_reflex (intersect,(normal -ray.get_direction() )+normal.unit(),ray.get_color());
	return (rayon_reflex);
}

double Surface::CoeffPhong(const Rayon& ray, Light source,double coeffPhong,Camera ca)
{
	vector RayOrigin = ray.get_origin();
	vector RayDirection = ray.get_direction();
	double coeff = Surface::lambda(ray);
	if (coeff >0)
	{vector PointIntersect = RayOrigin + coeff* RayDirection;
	//std::cout << (PointIntersect.x) <<" "<<(PointIntersect.y) <<" "<< (PointIntersect.z)<<std::endl;
	vector LumIncident = (source.get_origin() - PointIntersect).unit();
	//std::cout<<(LumIncident|direction.unit());
	return ((LumIncident|direction.unit())*(coeffPhong));}
	else
	{
		return 0;
	}
}
