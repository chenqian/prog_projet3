#include "obj.hpp"
#include "color.hpp"
using namespace rt;

Obj::Obj()
{
	obj_color = color::WHITE;
}

Obj::Obj(const Obj& obj)
{
	obj_color = obj.obj_color;
}

Obj::Obj (color obj_col)
{
	this->obj_color = obj_col;
}

color Obj::get_color ()
{
	return obj_color; 
}
