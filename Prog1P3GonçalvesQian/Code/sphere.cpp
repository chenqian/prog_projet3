#include <iostream>
#include "obj.hpp"
#include "sphere.hpp"
#include "rayon.hpp"
#include "vector.hpp"
#include "cmath"
#include "algorithm"
using namespace rt;
using namespace std;
Sphere :: Sphere (rt::vector vec, double ray) :Obj::Obj(color::WHITE)
{
	centre = vec;
	rayon = ray;
}

Sphere :: Sphere (rt::vector centre, double rayon, rt::color couleur) : Obj::Obj(couleur)
{
	this->centre = centre;
	this->rayon = rayon;
}

rt::vector Sphere :: get_centre()
{
	return centre;
}

double Sphere :: get_rayon()
{
	return rayon;
}

double Sphere::lambda(const Rayon& ray)
{
	vector orig (ray.get_origin());
	vector v (ray.get_direction());
	double dist2 = pow((centre - ray.get_origin()).norm(),2) - pow(((centre - ray.get_origin()) | ray.get_direction().unit()),2);
	if (dist2 <= pow(rayon,2))
	{
		double lambda1 = (centre.x*v.x+centre.y*v.y+centre.z*v.z-v.x*orig.x-v.y*orig.y-v.z*orig.z+sqrt(2*centre.x*pow(v.y,2)*orig.x+2*centre.x*pow(v.z,2)*orig.x+2*centre.y*pow(v.x,2)*orig.y+2*centre.y*pow(v.z,2)*orig.y+2*centre.z*pow(v.x,2)*orig.z+2*centre.z*pow(v.y,2)*orig.z+2*centre.x*centre.y*v.x*v.y+2*centre.x*centre.z*v.x*v.z-2*centre.x*v.x*v.y*orig.y-2*centre.x*v.x*v.z*orig.z+2*centre.y*centre.z*v.y*v.z-2*centre.y*v.x*v.y*orig.x-2*centre.y*v.y*v.z*orig.z-2*centre.z*v.x*v.z*orig.x-2*centre.z*v.y*v.z*orig.y+2*v.x*v.y*orig.x*orig.y+2*v.x*v.z*orig.x*orig.z+2*v.y*v.z*orig.y*orig.z-pow(centre.x,2)*pow(v.y,2)-pow(centre.x,2)*pow(v.z,2)-pow(centre.y,2)*pow(v.x,2)-pow(centre.y,2)*pow(v.z,2)-pow(centre.z,2)*pow(v.x,2)-pow(centre.z,2)*pow(v.y,2)+pow(rayon,2)*pow(v.x,2)+pow(rayon,2)*pow(v.y,2)+pow(rayon,2)*pow(v.z,2)-pow(v.x,2)*pow(orig.y,2)-pow(v.x,2)*pow(orig.z,2)-pow(v.y,2)*pow(orig.x,2)-pow(v.y,2)*pow(orig.z,2)-pow(v.z,2)*pow(orig.x,2)-pow(v.z,2)*pow(orig.y,2)))/(pow(v.x,2)+pow(v.y,2)+pow(v.z,2));
		double lambda2 = -(-centre.x*v.x-centre.y*v.y-centre.z*v.z+v.x*orig.x+v.y*orig.y+v.z*orig.z+sqrt(2*centre.x*pow(v.y,2)*orig.x+2*centre.x*pow(v.z,2)*orig.x+2*centre.y*pow(v.x,2)*orig.y+2*centre.y*pow(v.z,2)*orig.y+2*centre.z*pow(v.x,2)*orig.z+2*centre.z*pow(v.y,2)*orig.z+2*centre.x*centre.y*v.x*v.y+2*centre.x*centre.z*v.x*v.z-2*centre.x*v.x*v.y*orig.y-2*centre.x*v.x*v.z*orig.z+2*centre.y*centre.z*v.y*v.z-2*centre.y*v.x*v.y*orig.x-2*centre.y*v.y*v.z*orig.z-2*centre.z*v.x*v.z*orig.x-2*centre.z*v.y*v.z*orig.y+2*v.x*v.y*orig.x*orig.y+2*v.x*v.z*orig.x*orig.z+2*v.y*v.z*orig.y*orig.z-pow(centre.x,2)*pow(v.y,2)-pow(centre.x,2)*pow(v.z,2)-pow(centre.y,2)*pow(v.x,2)-pow(centre.y,2)*pow(v.z,2)-pow(centre.z,2)*pow(v.x,2)-pow(centre.z,2)*pow(v.y,2)+pow(rayon,2)*pow(v.x,2)+pow(rayon,2)*pow(v.y,2)+pow(rayon,2)*pow(v.z,2)-pow(v.x,2)*pow(orig.y,2)-pow(v.x,2)*pow(orig.z,2)-pow(v.y,2)*pow(orig.x,2)-pow(v.y,2)*pow(orig.z,2)-pow(v.z,2)*pow(orig.x,2)-pow(v.z,2)*pow(orig.y,2)))/(pow(v.x,2)+pow(v.y,2)+pow(v.z,2));
		return (min(lambda1,lambda2));
	}
	else
	{ 
		return 0;
	}
}


//calculer l'intersection d'un rayon avec le Sphere
vector Sphere::IntersectRay (const Rayon& ray)
{
	vector orig (ray.get_origin());
	vector v (ray.get_direction());
	double coeff = Sphere::lambda(ray);
	if (coeff == 0)
	{
		return vector (0,0,0);
	}
	else
	{
		return (orig + v* coeff);
	};
}


//calculer la couleur du rayon
//x,y,z repere la position de la source de lumiere
//posx posy repere la position du pixel sur l'ecran
double Sphere::CoeffReflex(const Rayon& ray, Light source)
{
	vector PointInsect (Sphere::IntersectRay(ray));
	double coeff = 0;
	//cout<< PointInsect.x<<","<<PointInsect.y<<","<<PointInsect.z<<"          ";
	if (PointInsect == vector(0,0,0))
	{
		coeff = 0;
	}
	else
	{
		vector rayLum ((source.get_origin() - PointInsect).unit());
		vector rayNormal ((PointInsect - centre).unit());
		coeff = (rayLum | rayNormal);
		//cout<<coeff<<"  ";
	};
	//cout<<coeff<<"    ";
	if (coeff <= 0){coeff = 0;};
	
	//cout<<couleur<<"  ";
	return (coeff);
}

Rayon Sphere::reflex_rayon (const Rayon& ray)
{
	vector intersect (this->Sphere::IntersectRay(ray));
	vector normal (intersect - centre);
	Rayon rayon_reflex (intersect,(normal - ray.get_direction())+normal,ray.get_color());
	return (rayon_reflex);
}

double Sphere::CoeffPhong(const Rayon& ray, Light source,double coeffPhong,Camera ca)
{
	vector PointInsect (Sphere::IntersectRay(ray));
	double coeff = 0;
	//cout<< PointInsect.x<<","<<PointInsect.y<<","<<PointInsect.z<<"          ";
	if (PointInsect == vector(0,0,0))
	{
		coeff = 0;
	}
	else
	{
		vector rayLum ((source.get_origin() - PointInsect).unit());
		vector rayNormal ((PointInsect - centre).unit());
		coeff = (rayLum | rayNormal)*(coeffPhong);
		//cout<<coeff<<"  ";
	};
	//cout<<coeff<<"    ";
	if (coeff <= 0){coeff = 0;};
	
	//cout<<couleur<<"  ";
	return (coeff);
}
