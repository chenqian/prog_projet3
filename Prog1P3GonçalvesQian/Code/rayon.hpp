#pragma once
#include "vector.hpp"
#include "color.hpp"

namespace rt {

    /* La classe Rayon sera représentée par :
        - l'origin du Rayon (la Camera)
        - la direction du Rayon sous forme de vecteur
       Si on appelle A la position de la Camera et V la direction, le Rayon sera donc la demi-droite
       d'équation A+tV, avec t>0
    */

    class Rayon
    {
        private:
            rt::vector origin;
            rt::vector direction;
            rt::color RayColor;
        public:
			Rayon();
            Rayon(rt::vector origin, rt::vector direction, rt::color couleur);
            rt::vector get_origin() const;
            rt::vector get_direction() const;
            rt::color get_color() const;
            void set_color(color RayColor);
    };
}
