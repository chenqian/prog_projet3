#pragma once
#include "vector.hpp"
#include "color.hpp"
#include "rayon.hpp"
#include "light.hpp"
#include "camera.hpp"
#include "obj.hpp"
using namespace rt;

namespace rt{
class Sphere : public Obj
{
	private: 
		vector centre;
		double rayon;
	public:
		Sphere (vector centre, double rayon);
		Sphere (vector centre, double rayon, color couleur);
		vector get_centre ();
		double get_rayon ();
		double lambda(const Rayon& ray);
		vector IntersectRay (const Rayon& ray);
		double CoeffReflex(const Rayon& ray, Light source);
		Rayon reflex_rayon (const Rayon& ray);
		double CoeffPhong(const Rayon& ray, Light source,double coeff,Camera ca);
};
}
